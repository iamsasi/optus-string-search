package com.example.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProjectUtils {

	/**
	 * Compare each word with the file and builds JSON response
	 * 
	 * @param line
	 * @throws JSONException
	 */
	public JSONArray findOccurance(String inputFileContent, String word) {
		int count = StringUtils.countMatches(inputFileContent, word);
		JSONObject data = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		try {
			data.put(word, count);
			jsonArray.put(data);
		} catch (JSONException e) {
			e.printStackTrace();
			// TODO logger code
			jsonArray.put("Error while processing data");
		}
		return jsonArray;
	}
	/**
	 * Counts word frequency
	 * @param file
	 * @param cutOff
	 */
	public JSONArray countFrequency(String fileName, int cutOff) {
		JSONArray jsonArray = new JSONArray();
		Scanner input = null;
		try {
			input = new Scanner(getFile(fileName));
		} catch (Exception e) {
			// TODO logger code
			e.printStackTrace();
			if (input != null)
				input.close();
			return jsonArray;
		}
		// count occurrences
		Map<String, Integer> wordCounts = new TreeMap<String, Integer>();
		while (input.hasNext()) {
			String next = input.next().toLowerCase();
			if (!wordCounts.containsKey(next)) {
				wordCounts.put(next, 1);
			} else {
				wordCounts.put(next, wordCounts.get(next) + 1);
			}
		}

		LinkedHashMap<String, Integer> sortedMap = wordCounts.entrySet().stream().sorted(Entry.comparingByValue())
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		LinkedHashMap<String, Integer> reverseSortedMap = reverseMap(sortedMap);
		int counter = 0;
		for (String word : reverseSortedMap.keySet()) {
			Integer count = wordCounts.get(word);
			counter++;
			if (counter <= cutOff) {
				//System.out.println(word + "|" + count);
				String value = word + "|" + count;
				jsonArray.put(value);
			}
		}
		return jsonArray;
	}

	/**
	 * 
	 * @param toReverse
	 * @return
	 */
	private <T, Q> LinkedHashMap<T, Q> reverseMap(LinkedHashMap<T, Q> toReverse) {
		LinkedHashMap<T, Q> reversedMap = new LinkedHashMap<>();
		List<T> reverseOrderedKeys = new ArrayList<>(toReverse.keySet());
		Collections.reverse(reverseOrderedKeys);
		reverseOrderedKeys.forEach((key) -> reversedMap.put(key, toReverse.get(key)));
		return reversedMap;
	}
	
	
	/**
	 * Read file
	 * 
	 * @param fileName
	 * @return
	 */
	public String getFileContent(String fileName) {
		StringBuilder result = new StringBuilder("");
		try (Scanner scanner = new Scanner(getFile(fileName))) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}
	
	
	public File getFile(String fileName){
		// Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		return file;
	}

}
