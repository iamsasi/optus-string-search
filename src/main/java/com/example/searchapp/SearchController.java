/**
 * Coding test for Optus.
 * Task 1: Search the following texts, which will return the counts respectively.
 * As a user, I will be able to execute the following curl command and returning results in the json format:
 * 
 * Sample Request
 *  curl http://host/counter-api/search -H"Authorization: Basic b3B0dXM6Y2FuZGlkYXRlcw==" - d’{“searchText”:
 *  [“Duis”, “Sed”, “Donec”, “Augue”, “Pellentesque”, “123”]}’ -H"Content-Type: application/json" –X POST
 * 
 * Result in JSON:
 * {"counts": [{"Duis": 11}, {"Sed": 16}, {"Donec": 8}, {"Augue": 7}, {"Pellentesque": 6},{"123": 0}]} 
 * 
 * @author Sasi Nataraja
 * @version 1.0
 * @since   03 Aug 2018 
 *  
 */

package com.example.searchapp;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.util.ProjectUtils;

@RestController
public class SearchController {

	private JSONArray jsonArray;
	private String inputFileContent;
	private ProjectUtils projUtil;
	private String fileName ="input.txt";
	
	public SearchController() {
		jsonArray = new JSONArray();
		projUtil = new ProjectUtils();
		inputFileContent = projUtil.getFileContent(fileName);
	}

	@RequestMapping(value = "/counter-api/search", method = RequestMethod.POST)
	@ResponseBody
	public String searchbyText(@RequestBody String payload) throws Exception {
		JSONObject json = new JSONObject(payload);
		Iterator<String> it = json.keys();
		JSONObject jsonResponse = new JSONObject();
		JSONArray jsonArrayProcessed = null;
		while (it.hasNext()) {
			String key = it.next().toString(); 
			JSONArray jsonArray = json.getJSONArray(key); // search text values
			jsonArrayProcessed = processInput(jsonArray);
		}
		jsonResponse.put("counts", jsonArrayProcessed);
		return jsonResponse.toString();
	}
	
	   @RequestMapping (value = "/top/{val}", method = RequestMethod.GET, produces="text/csv")
	   @ResponseBody
	    public String searchbyRank(@PathVariable("val") int val) {
		    JSONObject jsonResponse = new JSONObject();
		    JSONArray jsonArray = projUtil.countFrequency(fileName,val);
			return jsonArray.toString();
	    }

	/**
	 * Read each word in JSON array and process it
	 * 
	 * @param array
	 */
	private JSONArray processInput(JSONArray array) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < array.length(); i++) {
			try {
				String value = array.getString(i);
				value = value.substring(1, value.length() -1);
				jsonArray.put(projUtil.findOccurance(inputFileContent,value));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return jsonArray;
	}
	



	
}